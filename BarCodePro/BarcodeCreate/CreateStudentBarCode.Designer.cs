﻿namespace BarcodeCreate
{
    partial class CreateStudentBarCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateStudentBarCode));
            this.btnSeach = new System.Windows.Forms.Button();
            this.cboStudent = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblClass = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSignle = new System.Windows.Forms.Button();
            this.lblTotal = new System.Windows.Forms.Label();
            this.btnNoCheck = new System.Windows.Forms.Button();
            this.btnAllCheck = new System.Windows.Forms.Button();
            this.btnPrintPer = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.cboClass = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.grpStudent = new System.Windows.Forms.GroupBox();
            this.flpStudentList = new System.Windows.Forms.FlowLayoutPanel();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.skinEngine1 = new Sunisoft.IrisSkin.SkinEngine(((System.ComponentModel.Component)(this)));
            this.btnSelectDs = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.grpStudent.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSeach
            // 
            this.btnSeach.Location = new System.Drawing.Point(991, 10);
            this.btnSeach.Name = "btnSeach";
            this.btnSeach.Size = new System.Drawing.Size(87, 27);
            this.btnSeach.TabIndex = 0;
            this.btnSeach.Text = "查询";
            this.btnSeach.UseVisualStyleBackColor = true;
            this.btnSeach.Click += new System.EventHandler(this.btnSeach_Click);
            // 
            // cboStudent
            // 
            this.cboStudent.FormattingEnabled = true;
            this.cboStudent.Location = new System.Drawing.Point(67, 10);
            this.cboStudent.Name = "cboStudent";
            this.cboStudent.Size = new System.Drawing.Size(211, 22);
            this.cboStudent.TabIndex = 1;
            this.cboStudent.SelectedIndexChanged += new System.EventHandler(this.cboStudent_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 2;
            this.label1.Text = "学校";
            // 
            // lblClass
            // 
            this.lblClass.AutoSize = true;
            this.lblClass.Location = new System.Drawing.Point(327, 15);
            this.lblClass.Name = "lblClass";
            this.lblClass.Size = new System.Drawing.Size(35, 14);
            this.lblClass.TabIndex = 4;
            this.lblClass.Text = "班级";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSelectDs);
            this.panel1.Controls.Add(this.btnSignle);
            this.panel1.Controls.Add(this.lblTotal);
            this.panel1.Controls.Add(this.btnNoCheck);
            this.panel1.Controls.Add(this.btnAllCheck);
            this.panel1.Controls.Add(this.btnPrintPer);
            this.panel1.Controls.Add(this.btnPrint);
            this.panel1.Controls.Add(this.cboClass);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtUserName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnSeach);
            this.panel1.Controls.Add(this.lblClass);
            this.panel1.Controls.Add(this.cboStudent);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1197, 85);
            this.panel1.TabIndex = 5;
            // 
            // btnSignle
            // 
            this.btnSignle.Location = new System.Drawing.Point(856, 49);
            this.btnSignle.Name = "btnSignle";
            this.btnSignle.Size = new System.Drawing.Size(200, 27);
            this.btnSignle.TabIndex = 12;
            this.btnSignle.Text = "打印设置(份数/每人)";
            this.btnSignle.UseVisualStyleBackColor = true;
            this.btnSignle.Click += new System.EventHandler(this.btnSignle_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(327, 58);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(35, 14);
            this.lblTotal.TabIndex = 11;
            this.lblTotal.Text = "学校";
            // 
            // btnNoCheck
            // 
            this.btnNoCheck.Location = new System.Drawing.Point(191, 52);
            this.btnNoCheck.Name = "btnNoCheck";
            this.btnNoCheck.Size = new System.Drawing.Size(87, 27);
            this.btnNoCheck.TabIndex = 10;
            this.btnNoCheck.Text = "反选";
            this.btnNoCheck.UseVisualStyleBackColor = true;
            this.btnNoCheck.Click += new System.EventHandler(this.btnNoCheck_Click);
            // 
            // btnAllCheck
            // 
            this.btnAllCheck.Location = new System.Drawing.Point(67, 52);
            this.btnAllCheck.Name = "btnAllCheck";
            this.btnAllCheck.Size = new System.Drawing.Size(87, 27);
            this.btnAllCheck.TabIndex = 9;
            this.btnAllCheck.Text = "全选";
            this.btnAllCheck.UseVisualStyleBackColor = true;
            this.btnAllCheck.Click += new System.EventHandler(this.btnAllCheck_Click);
            // 
            // btnPrintPer
            // 
            this.btnPrintPer.Location = new System.Drawing.Point(745, 52);
            this.btnPrintPer.Name = "btnPrintPer";
            this.btnPrintPer.Size = new System.Drawing.Size(87, 27);
            this.btnPrintPer.TabIndex = 8;
            this.btnPrintPer.Text = "打印预览";
            this.btnPrintPer.UseVisualStyleBackColor = true;
            this.btnPrintPer.Click += new System.EventHandler(this.btnPrintPer_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(1104, 11);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(87, 27);
            this.btnPrint.TabIndex = 7;
            this.btnPrint.Text = "直接打印";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // cboClass
            // 
            this.cboClass.FormattingEnabled = true;
            this.cboClass.Location = new System.Drawing.Point(393, 12);
            this.cboClass.Name = "cboClass";
            this.cboClass.Size = new System.Drawing.Size(211, 22);
            this.cboClass.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(642, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 14);
            this.label2.TabIndex = 6;
            this.label2.Text = "学生姓名";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(726, 12);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(239, 23);
            this.txtUserName.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.grpStudent);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 85);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1197, 657);
            this.panel2.TabIndex = 6;
            // 
            // grpStudent
            // 
            this.grpStudent.Controls.Add(this.flpStudentList);
            this.grpStudent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpStudent.Location = new System.Drawing.Point(0, 0);
            this.grpStudent.Name = "grpStudent";
            this.grpStudent.Size = new System.Drawing.Size(1197, 657);
            this.grpStudent.TabIndex = 2;
            this.grpStudent.TabStop = false;
            this.grpStudent.Text = "学生";
            // 
            // flpStudentList
            // 
            this.flpStudentList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpStudentList.Location = new System.Drawing.Point(3, 19);
            this.flpStudentList.Name = "flpStudentList";
            this.flpStudentList.Size = new System.Drawing.Size(1191, 635);
            this.flpStudentList.TabIndex = 0;
            // 
            // skinEngine1
            // 
            this.skinEngine1.SerialNumber = "";
            this.skinEngine1.SkinFile = null;
            // 
            // btnSelectDs
            // 
            this.btnSelectDs.Location = new System.Drawing.Point(1079, 49);
            this.btnSelectDs.Name = "btnSelectDs";
            this.btnSelectDs.Size = new System.Drawing.Size(116, 27);
            this.btnSelectDs.TabIndex = 13;
            this.btnSelectDs.Text = "使用环境设置";
            this.btnSelectDs.UseVisualStyleBackColor = true;
            this.btnSelectDs.Click += new System.EventHandler(this.btnSelectDs_Click);
            // 
            // CreateStudentBarCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1197, 742);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CreateStudentBarCode";
            this.Text = "条形码打印系统";
            this.Load += new System.EventHandler(this.CreateStudentBarCode_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.grpStudent.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSeach;
        private System.Windows.Forms.ComboBox cboStudent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblClass;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.GroupBox grpStudent;
        private System.Windows.Forms.FlowLayoutPanel flpStudentList;
        private System.Windows.Forms.ComboBox cboClass;
        private System.Windows.Forms.Button btnPrintPer;
        private System.Windows.Forms.Button btnPrint;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
     
        private System.Windows.Forms.Button btnNoCheck;
        private System.Windows.Forms.Button btnAllCheck;
        private System.Windows.Forms.Label lblTotal;
        private Sunisoft.IrisSkin.SkinEngine skinEngine1;
        private System.Windows.Forms.Button btnSignle;
        private System.Windows.Forms.Button btnSelectDs;
    }
}