﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BarcodeCreate
{
   public class PdfHelper
    {

       public static string CreateDetailFile(List<string> imgList, int num)
       {

           // 生成具体文件
           string temp =CommonHelp.GetCurrentRootRoute() + @"\Template\barcodeTemp.pdf";
           string newfile = Application.StartupPath + @"\temp\" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + "_" + num.ToString() + ".pdf";
           Dictionary<string, string> dir = new Dictionary<string, string>();
           GeneratorPdf(temp, newfile, dir, imgList);
           return newfile;
       }

       public static void MergePDFFiles(List<string> fileList, string outMergeFile)
       {


           // string filetext = Application.StartupPath + @"\testinfo.pdf";
           string filetext = outMergeFile;
           Document document = new Document();
           PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filetext, FileMode.Create));
           document.Open();
           for (int j = 0; j < fileList.Count; j++)
           {
               PdfReader reader = new PdfReader(fileList[j]);
               int n = reader.NumberOfPages;
               PdfImportedPage page;
               for (int i = 1; i <= n; i++)
               {
                   page = writer.GetImportedPage(reader, i);
                   iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(page);
                   image.SetAbsolutePosition(0, 0);
                   document.Add(image);
                   document.NewPage();
               }
           }
           document.Close();

       }





       public static void GeneratorPdf(String templatePath, String targetPath, Dictionary<string, string> parameters, List<string> imageListPath)
       {

           // 读取模板文件
           PdfReader pdfReader = new PdfReader(templatePath);

           PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(targetPath, FileMode.Create));
           // 提取pdf中的表单
           AcroFields form = pdfStamper.AcroFields;
           BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
           form.AddSubstitutionFont(bf);

           // 通过域名获取所在页和坐标，左下角为起点

           int pageNo = form.GetFieldPositions("img1").First().page;
           IList<AcroFields.FieldPosition> list = form.GetFieldPositions("img1");
           int i = 0;
           foreach (AcroFields.FieldPosition info in list)
           {
               iTextSharp.text.Rectangle signRect = info.position;
               float x = signRect.GetLeft(0);
               float y = signRect.GetBottom(10);
               if (i >= imageListPath.Count)
               {
                   break;
               }

               // 读图片
               iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageListPath[i]);
               // 获取操作的页面
               PdfContentByte under = pdfStamper.GetOverContent(pageNo);
               // 根据域的大小缩放图片
               image.ScaleToFit(signRect);
               // 添加图片
               image.SetAbsolutePosition(x, y);
               under.AddImage(image);
               i++;
           }


           // 添加文字
           if (parameters.Count > 0)
           {
               foreach (KeyValuePair<string, string> parameter in parameters)
               {
                   form.SetField(parameter.Key, parameter.Value);

               }
           }

           pdfStamper.FormFlattening = true;

           pdfStamper.Close();
           pdfReader.Close();
       }

    }
}
