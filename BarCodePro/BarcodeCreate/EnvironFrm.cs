﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;

namespace BarcodeCreate
{
    public enum SysConfigEnvironment
    {
        DBConnectionTest=0,
        DBConnection=1
    }
    public partial class EnvironFrm : Form
    {
     
        public EnvironFrm()
        {
            InitializeComponent();
          
            this.Load += delegate { GetDefaultData(); };
        }

      
      

        private void UpdateData(SysConfigEnvironment sysConfig)
        {
            string filePath = Application.StartupPath + @"\\SysConfig.xml";
            //定义并从xml文件中加载节点（根节点）
            XElement rootNode = XElement.Load(filePath);
           rootNode.Element("SysDefault").Value=sysConfig.ToString();
           rootNode.Save(filePath);
           GetDataConnectionString();
           MessageBox.Show("保存成功,请重新启动!", "Info");
        }

        public static string GetDataConnectionString()
        {
            string dbConnection = string.Empty;
            string filePath = Application.StartupPath + @"\\SysConfig.xml";
            //定义并从xml文件中加载节点（根节点）
            XElement rootNode = XElement.Load(filePath);
            string defaultConnection= rootNode.Element("SysDefault").Value.Trim();

             IEnumerable<XElement> targetNodes = from target in rootNode.Descendants("environment")
                                                 where target.Attribute("key").Value ==defaultConnection 
                                                    
                                                    select target;
                //遍历所获得的目标节点（集合）
                foreach (XElement node in targetNodes)
                {
                   
                    dbConnection = node.Value;
                    break;
                }
                return dbConnection;
        }


        private void GetDefaultData( )
        {
            string filePath = Application.StartupPath + @"\\SysConfig.xml";
            //定义并从xml文件中加载节点（根节点）
            XElement rootNode = XElement.Load(filePath);
           string data=  rootNode.Element("SysDefault").Value.Trim();
           if (data.Equals(SysConfigEnvironment.DBConnectionTest.ToString()))
           {
               rdoTest.Checked = true;
           }
           else
           {
               rdoTryIt.Checked = true;
           }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.rdoTest.Checked)
            {
                UpdateData(SysConfigEnvironment.DBConnectionTest);
            }
            else
            {
                UpdateData(SysConfigEnvironment.DBConnection);
            }
        }

    
    }
}
