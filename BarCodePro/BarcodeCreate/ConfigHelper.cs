﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace BarcodeCreate
{
   public class ConfigHelper
    {
        #region 字段或属性

        /// <summary>
        /// 当前工作目录路径
        /// </summary>
        public static string strPath_1 = Directory.GetCurrentDirectory();

        #endregion

        #region 读取配置信息的方法

        /// <summary>
        /// 读取配置信息的方法
        /// </summary>
        /// <param name="key">配置信息键</param>
        /// <returns>配置信息值</returns>
        public static string GetAppSetting(string key)
        {
            return GetAppSetting(key, true);
        }

        /// <summary>
        /// 读取配置信息的方法
        /// </summary>
        /// <param name="key">配置信息键</param>
        /// <param name="throwException">配置文件里没有该配置时是否抛出异常</param>
        /// <returns>配置信息值</returns>
        public static string GetAppSetting(string key, bool throwException)
        {
            try
            {
                return ConfigurationManager.AppSettings[key];
            }
            catch
            {
                //if (throwException)
                //{
                //    //throw new Exception("没有在配置文件里找到名为'" + key + "'的配置信息。");
                //}

                //else
                return "";
            }
        }

        #endregion
    }
}
