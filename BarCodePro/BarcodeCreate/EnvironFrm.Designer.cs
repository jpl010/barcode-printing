﻿namespace BarcodeCreate
{
    partial class EnvironFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdoTest = new System.Windows.Forms.RadioButton();
            this.rdoTryIt = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rdoTest
            // 
            this.rdoTest.AutoSize = true;
            this.rdoTest.Location = new System.Drawing.Point(33, 24);
            this.rdoTest.Name = "rdoTest";
            this.rdoTest.Size = new System.Drawing.Size(71, 16);
            this.rdoTest.TabIndex = 0;
            this.rdoTest.TabStop = true;
            this.rdoTest.Text = "测试环境";
            this.rdoTest.UseVisualStyleBackColor = true;
            // 
            // rdoTryIt
            // 
            this.rdoTryIt.AutoSize = true;
            this.rdoTryIt.Location = new System.Drawing.Point(33, 74);
            this.rdoTryIt.Name = "rdoTryIt";
            this.rdoTryIt.Size = new System.Drawing.Size(71, 16);
            this.rdoTryIt.TabIndex = 1;
            this.rdoTryIt.TabStop = true;
            this.rdoTryIt.Text = "正试环境";
            this.rdoTryIt.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.rdoTryIt);
            this.panel1.Controls.Add(this.rdoTest);
            this.panel1.Location = new System.Drawing.Point(59, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(228, 153);
            this.panel1.TabIndex = 2;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(54, 118);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // EnvironFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 218);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EnvironFrm";
            this.Text = "系统环境设置";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton rdoTest;
        private System.Windows.Forms.RadioButton rdoTryIt;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSave;
    }
}