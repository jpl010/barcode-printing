﻿using AxAcroPDFLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace BarcodeCreate
{
    public partial class FrmSingle : Form
    {

        public string SchoolName { get; set; }
        public string ClassName { get; set; }
        public string StudentIdList { get; set; }

        public int Current = 0;
        public List<string> CurrentFilePath = new List<string>();
        // 学号
        public Dictionary<string, string> StuNum { get; set; }
        // 学生名字
        public Dictionary<string, string> StuName { get; set; }
        public FrmSingle()
        {
            InitializeComponent();
            this.lblClassNametxt.Text = "";
            this.txtNum.MaxLength = 80;
            // 防止线程调用窗体是发生争用等错误
            CheckForIllegalCrossThreadCalls = false;
            this.txtNum.KeyUp += delegate 
            {
                CurrentFilePath.Clear();
            };
        }

        public FrmSingle(string SchoolName, string ClassName, string StudentIdList,Dictionary<string, string> StuNum, Dictionary<string, string> StuName)
            : this()
        {
            this.SchoolName = SchoolName;
            this.ClassName = ClassName;
            this.StudentIdList = StudentIdList;
            this.StuName = StuName;
            this.StuNum = StuNum;
        }


        // 打印
        private void btnPrint_Click(object sender, EventArgs e)
        {
            btnPrint.Enabled = false;
            if (CurrentFilePath.Count > 0)
            {
                for (int i = 0; i < CurrentFilePath.Count; i++)
                {
                    AxAcroPDF ac = new AxAcroPDF();
                    ((System.ComponentModel.ISupportInitialize)(ac)).BeginInit();
                    this.Controls.Add(ac);
                    ((System.ComponentModel.ISupportInitialize)(ac)).EndInit();
                    ac.LoadFile(CurrentFilePath[i].ToString());
                    ac.printAll();

                }
            }
            else
            {
                List<string> fileList = new List<string>();
                if (string.IsNullOrEmpty(this.txtNum.Text.Trim()))
                {
                    MessageBox.Show("请输入份数", "信息提示");
                    return;
                }
                if (!ValidteData(this.txtNum.Text.Trim()))
                {
                    MessageBox.Show("您输入的数值有误,请输入份数为正整数数字!", "信息提示");
                    return;
                }
                int num = 0;
                Int32.TryParse(this.txtNum.Text.Trim(), out num);
                fileList = CommonHelp.PrintBarCode(this.StudentIdList, SchoolName, ClassName, StuName, StuNum, "", num);
                CurrentFilePath = fileList;
                for (int i = 0; i < fileList.Count; i++)
                {
                    AxAcroPDF ac = new AxAcroPDF();
                    ((System.ComponentModel.ISupportInitialize)(ac)).BeginInit();
                    this.Controls.Add(ac);
                    ((System.ComponentModel.ISupportInitialize)(ac)).EndInit();
                    ac.LoadFile(fileList[i].ToString());
                    ac.printAll();

                }
            }
          
            MessageBox.Show("打印成功!", "信息提示");
            btnPrint.Enabled = true; ;
        }


        // 保存
        private void btnSave_Click(object sender, EventArgs e)
        {
           
            if (CurrentFilePath.Count > 0)
            {
                BathSave(CurrentFilePath);
               
            }
            else
            {
                List<string> fileList = new List<string>();
                if (string.IsNullOrEmpty(this.txtNum.Text.Trim()))
                {
                    MessageBox.Show("请输入份数", "信息提示");
                    return;
                }
                if (!ValidteData(this.txtNum.Text.Trim()))
                {
                    MessageBox.Show("您输入的数值有误,请输入份数为正整数数字!", "信息提示");
                    return;
                }

                int num = 0;
                Int32.TryParse(this.txtNum.Text.Trim(), out num);
                fileList = CommonHelp.PrintBarCode(this.StudentIdList, SchoolName, ClassName, StuName, StuNum, "", num);
                CurrentFilePath = fileList;

                BathSave(fileList);

            }
        }

        public void BathSave(List<string> listFile)
        {

            int curPage = 1;
            string newfilename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "_" + curPage.ToString() + ".pdf";
            if (listFile != null && listFile.Count > 0)
            {
                if (listFile.Count == 1)
                {
                    newfilename = listFile[0].ToString();
                }
                else
                {
                    PdfHelper.MergePDFFiles(listFile, newfilename);
                }

                string localFilePath = String.Empty;
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                //设置文件类型  
                saveFileDialog1.Filter = "PDF文件|*.pdf";
                //设置文件名称：
                saveFileDialog1.FileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "_news.pdf";
                curPage++;

                //设置默认文件类型显示顺序  
                saveFileDialog1.FilterIndex = 2;

                //保存对话框是否记忆上次打开的目录  
                saveFileDialog1.RestoreDirectory = true;

                //点了保存按钮进入  
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    File.Copy(newfilename, saveFileDialog1.FileName, true);
                    MessageBox.Show("保存成功!", "信息提示");
                }

            }

        }

        public  bool ValidteData(string validteString)
        {
            string pattern = "^[+]?\\d+$";
            return PublicMethod(pattern, validteString);
        }

        private  bool PublicMethod(string pattern, string validteString)
        {
            Regex reg = new Regex(pattern);
            Match m = reg.Match(validteString);
            return m.Success;
        }

        private void FrmSingle_Load(object sender, EventArgs e)
        {
            this.groupBox1.Text = SchoolName;
            this.lblClassNametxt.Text= ClassName;

            if (!string.IsNullOrEmpty(StudentIdList))
            {
                string sidArray = StudentIdList.Trim(',');
                var sidList = sidArray.Split(',').Where(x =>
                     x.Split(new char[] { ',' }).Count() > 0);

                int interval = 230;
                int pint = 0;
                foreach (string sid in sidList)
                {
                    Label lbl = new Label();
                    string name="";
                    StuName.TryGetValue(sid, out name);
                    lbl.Text = name;
                    int xcoordinate = interval * pint + 86;
                    lbl.Location = new Point(xcoordinate, 43);
                    lbl.Size = new Size(150, 30);
                    flowLayoutPanel1.Controls.Add(lbl);
                   
                }
            }
           

        }
    }
}
