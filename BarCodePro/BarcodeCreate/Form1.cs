﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;

namespace BarcodeCreate
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.picBarcodeImg.Click += new EventHandler(pictureBoxInfo_Click);
          //  this.picBarcodeImg.ContextMenuStrip = cmsList;
            txtFileUrl.ReadOnly = true;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
           // Create();
            if (string.IsNullOrEmpty(this.txtUrl.Text.Trim()))
            {
                MessageBox.Show("请选择生成文件的路径!", "信息提示!");
                return;
            }
            if (!string.IsNullOrEmpty(this.txtBarcode.Text.Trim()))
            {
                // 1.设置条形码规格
                EncodingOptions encodeOption = new EncodingOptions();
                encodeOption.Height = 123; // 必须制定高度、宽度
                encodeOption.Width = 398;
                encodeOption.PureBarcode = true;


                // 2.生成条形码图片并保存
                ZXing.BarcodeWriter wr = new BarcodeWriter();
                wr.Options = encodeOption;
                wr.Format = BarcodeFormat.CODE_128; //  条形码规格
            
                Bitmap img = wr.Write(txtBarcode.Text.Trim());
                string filePath = this.txtUrl.Text.Trim() + "\\" + this.txtBarcode.Text.Trim() + ".png";
                img.Save(filePath, System.Drawing.Imaging.ImageFormat.Png);
                picBarcodeImg.Image = img;       
            }
            else
            {
                MessageBox.Show("请输入条形码内容!", "信息提示!", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
        }


        public void Create()
        {
            QrCodeEncodingOptions options = new QrCodeEncodingOptions();
            options.CharacterSet = "UTF-8";
            options.DisableECI = true; // Extended Channel Interpretation (ECI) 主要用于特殊的字符集。并不是所有的扫描器都支持这种编码。
            options.ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.H; // 纠错级别
            options.Width = 300;
            options.Height = 300;
            options.Margin = 1;

            BarcodeWriter writer = new BarcodeWriter();
            writer.Format = BarcodeFormat.CODE_128;
            writer.Options = options;

         
            using (Bitmap bmp = writer.Write("http://www.cftea.com")) // Write 具备生成、写入两个功能
            {
                MemoryStream ms = new MemoryStream();
                {
                    bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    string rmFilename = Application.StartupPath + @"\test.jpg";
                    bmp.Save(rmFilename);
                }
            }
          

        }

        private void pictureBoxInfo_Click(object sender, EventArgs e)
        {

            string rmFilename = Application.StartupPath + @"\test.jpg";
            Bitmap img = new Bitmap(rmFilename);   // 导入一张RGB彩色图片
            Rectangle lockRect = new Rectangle(0, 0, img.Width, img.Height);
            BitmapData imgData = img.LockBits(lockRect, ImageLockMode.ReadOnly, img.PixelFormat);

            byte[,] rband = new byte[img.Height, img.Width];   // 彩色图片的R、G、B三层分别构造一个二维数组
            byte[,] gband = new byte[img.Height, img.Width];
            byte[,] bband = new byte[img.Height, img.Width];
            int rowOffset = imgData.Stride - img.Width * 3;

            // 这里不用img.GetPixel方法，而采用效率更高的指针来获取<a href="https://www.baidu.com/s?wd=%E5%9B%BE%E5%83%8F%E5%83%8F%E7%B4%A0&tn=44039180_cpr&fenlei=mv6quAkxTZn0IZRqIHckPjm4nH00T1YLuH6dPhRznj6YuAndmy7h0ZwV5Hcvrjm3rH6sPfKWUMw85HfYnjn4nH6sgvPsT6KdThsqpZwYTjCEQLGCpyw9Uz4Bmy-bIi4WUvYETgN-TLwGUv3EnHRLn1mzn1ckrH0dPjfLrHbsn0" target="_blank" class="baidu-highlight">图像像素</a>点的值
            //unsafe
            //{
            //    byte* imgPtr = (byte*)imgData.Scan0.ToPointer();

            //    for (int i = 0; i < img.Height; ++i)
            //    {
            //        for (int j = 0; j < img.Width; ++j)
            //        {
            //            rband[i, j] = imgPtr[2];   // 每个像素的指针是按BGR的顺序存储的
            //            gband[i, j] = imgPtr[1];
            //            bband[i, j] = imgPtr[0];

            //            imgPtr += 3;   // 偏移一个像素
            //        }
            //        imgPtr += rowOffset;   // 偏移到下一行
            //    }
            //}

            img.UnlockBits(imgData);

         

            //Bitmap bitmap = new Bitmap(this.Width, this.Height);
            //DrawToBitmap(this, bitmap, new Rectangle(0, 0, this.Width, this.Height));

            //bool isSave = true;
            //SaveFileDialog saveImageDialog = new SaveFileDialog();
            //saveImageDialog.Title = "图片保存";
            //saveImageDialog.Filter = @"jpeg|*.jpg|bmp|*.bmp|gif|*.gif";
            //if (saveImageDialog.ShowDialog() == DialogResult.OK)
            //{
            //    string fileName = saveImageDialog.FileName.ToString();
            //    if (fileName != "" && fileName != null)
            //    {
            //        string fileExtName = fileName.Substring(fileName.LastIndexOf(".") + 1).ToString();
            //        System.Drawing.Imaging.ImageFormat imgformat = null;
            //        if (fileExtName != "")
            //        {
            //            switch (fileExtName)
            //            {
            //                case "jpg":
            //                    imgformat = System.Drawing.Imaging.ImageFormat.Jpeg;
            //                    break;
            //                case "bmp":
            //                    imgformat = System.Drawing.Imaging.ImageFormat.Bmp;
            //                    break;
            //                case "gif":
            //                    imgformat = System.Drawing.Imaging.ImageFormat.Gif;
            //                    break;
            //                default:
            //                    MessageBox.Show("只能存取为: jpg,bmp,gif 格式");
            //                    isSave = false;
            //                    break;
            //            }

            //        }
            //        //默认保存为JPG格式  
            //        if (imgformat == null)
            //        {
            //            imgformat = System.Drawing.Imaging.ImageFormat.Jpeg;
            //        }
            //        if (isSave)
            //        {
            //            try
            //            {
            //                bitmap.Save(fileName, imgformat);
            //                //MessageBox.Show("图片已经成功保存!");  
            //            }
            //            catch
            //            {
            //                MessageBox.Show("保存失败,你还没有截取过图片或已经清空图片!");
            //            }
            //        }
            //    }
            //}

        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hWnd, int msg, int wParam, int lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(HandleRef hWnd, int msg, IntPtr wParam, IntPtr lParam);

        [DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        public static extern bool BitBlt(HandleRef hDC, int x, int y, int nWidth, int nHeight, HandleRef hSrcDC, int xSrc, int ySrc, int dwRop);

        /// <summary>
        /// 支持呈现到指定的位图。
        /// </summary>
        public static Bitmap DrawToBitmap(Control control, Bitmap bitmap, Rectangle targetBounds)
        {
            if (bitmap == null)
            {
                throw new ArgumentNullException("bitmap");
            }
            if (((targetBounds.Width <= 0) || (targetBounds.Height <= 0)) || ((targetBounds.X < 0) || (targetBounds.Y < 0)))
            {
                throw new ArgumentException("targetBounds");
            }
            Bitmap image = new Bitmap(control.Width, control.Height, bitmap.PixelFormat);
            using (Graphics graphics = Graphics.FromImage(image))
            {
                IntPtr hdc = graphics.GetHdc();
                SendMessage(new HandleRef(control, control.Handle), 0x317, hdc, (IntPtr)30);
                using (Graphics graphics2 = Graphics.FromImage(bitmap))
                {
                    IntPtr handle = graphics2.GetHdc();
                    BitBlt(new HandleRef(graphics2, handle), 0, 0, control.Width, control.Height, new HandleRef(graphics, hdc), targetBounds.X, targetBounds.Y,0xcc0020);
                    graphics2.ReleaseHdcInternal(handle);
                }
                graphics.ReleaseHdcInternal(hdc);
            }
            return image;
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog path = new FolderBrowserDialog();
            path.ShowDialog();
            this.txtUrl.Text = path.SelectedPath;
        }

        private void btnBatch_Click(object sender, EventArgs e)
        {
            BarcodeReader reader = new BarcodeReader();
            reader.Options.CharacterSet = "UTF-8";
            using (Bitmap bmp = new Bitmap("D:\\qr.png"))
            {
                Result result = reader.Decode(bmp);
               
            }
        }


        public bool IsNumer(string input)
        {
            string pattern = "^[0-9]*$";
            Regex rx = new Regex(pattern);
            return rx.IsMatch(input);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();


            fileDialog.Filter = @"All Image Files|*.bmp;*.ico;*.gif;*.jpeg;*.jpg;*.png;*.tif;*.tiff|Windows Bitmap(*.bmp)|*.bmp|Windows Icon(*.ico)|*.ico|Graphics Interchange Format (*.gif)|(*.gif)|JPEG File Interchange Format (*.jpg)|*.jpg;*.jpeg|Portable Network Graphics (*.png)|*.png| Tag Image File Format (*.tif)|*.tif;*.tiff";
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                    try
                    {
                        string ectension = Path.GetExtension(fileDialog.FileName);
                        txtFileUrl.Text = fileDialog.FileName;
                        Bitmap img = ReadImageFile(fileDialog.FileName);
                    
                        BarcodeReader _barcodeReader = new BarcodeReader();
                        _barcodeReader.Options.PossibleFormats = new List<BarcodeFormat> { BarcodeFormat.CODE_128 };
                        _barcodeReader.Options.TryHarder = false;
                        var result = _barcodeReader.Decode(img);
                        if (result != null)
                        {
                            txtReadBarcode.Text = result.Text;

                            return;

                        }
                        _barcodeReader.Options.TryHarder = true;
                        result = _barcodeReader.Decode(img);
                        txtReadBarcode.Text = result != null ? result.Text : String.Empty;
                        if (String.IsNullOrEmpty(txtReadBarcode.Text))
                        {
                            img.Dispose();
                             txtReadBarcode.Text= GetBarCode(fileDialog.FileName);
                        }
                      
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("请选择正确的条形码文件", "信息提示!");
                        Console.WriteLine(ex.Message);
                    }
                }

                

        
        
            }




        public string GetBarCode(string imageFile)
        {
           
            string barcodeString = string.Empty;
            if (File.Exists(imageFile))
            {
                string rmFilename = Application.StartupPath + @"\test.jpg";
                Bitmap originBmp = new Bitmap(imageFile);
                int w = originBmp.Width * 3;
                int h = originBmp.Height * 3;
                Bitmap resizedBmp = new Bitmap(w, h);
                Graphics g = Graphics.FromImage(resizedBmp);
                //设置高质量插值法   
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                //设置高质量,低速度呈现平滑程度   
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                //消除锯齿 
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.DrawImage(originBmp, new Rectangle(0, 0, w, h), new Rectangle(0, 0, originBmp.Width, originBmp.Height), GraphicsUnit.Pixel);
                resizedBmp.Save(rmFilename);
                g.Dispose();
                resizedBmp.Dispose();
                originBmp.Dispose();
                Bitmap originBmps = new Bitmap(rmFilename);

                Bitmap map = new Bitmap(rmFilename);
                BarcodeReader _barcodeReader = new BarcodeReader();
                _barcodeReader.Options.PossibleFormats = new List<BarcodeFormat> { BarcodeFormat.CODE_128 };
                _barcodeReader.Options.TryHarder = false;
                var result = _barcodeReader.Decode(map);
                if (result != null)
                {
                    return  result.Text;

                  
                }
                _barcodeReader.Options.TryHarder = true;
                result = _barcodeReader.Decode(map);
                 barcodeString = result != null ? result.Text : String.Empty;
                 map.Dispose();
                return barcodeString;
            }
            return barcodeString;
        }
    

        /// <summary>
        /// 通过FileStream 来打开文件，这样就可以实现不锁定Image文件，到时可以让多用户同时访问Image文件
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Bitmap ReadImageFile(string path)
        {
            FileStream fs = File.OpenRead(path); //OpenRead
            int filelength = 0;
            filelength = (int)fs.Length; //获得文件长度 
            Byte[] image = new Byte[filelength]; //建立一个字节数组 
            fs.Read(image, 0, filelength); //按字节流读取 
            System.Drawing.Image result = System.Drawing.Image.FromStream(fs);
            fs.Close();
            Bitmap bit = new Bitmap(result);
            return bit;
        }

       

       
    }
}
